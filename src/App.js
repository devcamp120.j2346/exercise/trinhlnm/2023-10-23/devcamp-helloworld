import devcampReact from "./assets/images/devcampreact.png";
import {gDevcampReact, getPercentStudyingStudents} from "./info";

function App() {
  return (
    <div>
      <h1>{gDevcampReact.title}</h1>
      <img src={gDevcampReact.image} alt="Devcamp" width={500}></img>
      <p>Tỷ lệ sinh viên đang theo học: {getPercentStudyingStudents(gDevcampReact.studyingStudents, gDevcampReact.totalStudents)}%</p>
      <p>Sinh viên đăng ký học {getPercentStudyingStudents(gDevcampReact.studyingStudents, gDevcampReact.totalStudents) > 15 ? "nhiều" : "ít"}</p>

      <ul>
        {gDevcampReact.benefits.map((element, index) => {
          return <li key={index}>{element}</li>;
        })}
      </ul>

      <div>
        <img src={devcampReact} alt="Devcamp React" width={1000}></img>
      </div>
    </div>
  );
}

export default App;
